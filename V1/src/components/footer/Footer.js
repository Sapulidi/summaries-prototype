import { Component } from 'preact';
import ScrollEvent from 'react-onscroll';
import style from './style';
export default class Footer extends Component {

	constructor(props) {
		super(props);
		this.state = {
			scrollPercentage: 0,
			timer: 3,
			timerRunning: false,
			scrolledDown: false,
			timerClass: "hideTimer"
		}
		this.handleScrollCallback = this.handleScrollCallback.bind(this);
	}

	componentDidUpdate(prevState) {
		if(this.state.timerRunning !== prevState.timerRunning && this.state.scrolledDown){
			switch(this.state.timerRunning) {
				case true:
				this.countDown();
			}
		}
	}
	
	handleScrollCallback() {
        let h = document.documentElement, 
            b = document.body,
            st = 'scrollTop',
            sh = 'scrollHeight';

        let percent = (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;

        this.setState({
            scrollPercentage : Math.round(percent)
		})
		this.countDown();	
	}

	countDown() {
		if( this.state.scrollPercentage === 100 && !this.state.timerRunning && !this.state.scrolledDown) {
			this.setState({timerRunning : true, timerClass: "showTimer"})
			
			this.timer = setInterval(() => {
				if(this.state.timerRunning) {
					const newCount = this.state.timer - 1;
					this.setState(
						{timer: newCount >= 0 ? newCount : 0}
					);
				}
				if(this.state.timer === 0) {
					this.setState({scrolledDown : true})
					this.stopTimer();
				}
			}, 1000);
		}
	}

	stopTimer() {
		this.setState(
			{ timerRunning: false, timer: 3, scrollPercentage : 0, scrolledDown : false, timerClass: "hideTimer"}
	  	);
		clearInterval(this.timer);
		this.props.onNext();
	}

	render() {
		let divStyle = {
			width: `${this.state.scrollPercentage}%`
		}

		return(
			<footer class={style.footer}>

				<ScrollEvent handleScrollCallback={this.handleScrollCallback} />

				<div style={divStyle} data-color={`backgroundColor${this.props.headerColorClass}`}></div>

				<a onClick={this.props.onPrevious}>
					<img src="../../assets/img/chevron-left-solid.svg" />
					<span>{this.props.previousTopicTitle}</span>
				</a>

				<span className={`counter ${this.state.timerClass}`}>volgend topic in {this.state.timer} seconden</span>

				<a onClick={this.props.onNext}>
					<span>{this.props.nextTopicTitle}</span>
					<img src="../../assets/img/chevron-right-solid.svg" />
				</a>

			</footer>
		)
	}
};