import { Component } from 'preact';
import style from './style';

export default class Header extends Component {

	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			<header class={style.header}>
				<h1 data-color={`color${this.props.headerColorClass}`}>{this.props.topic.title}</h1>
				<h2>{this.props.topic.readTime} minuten leestijd</h2>
			</header>
		)
	}
};
