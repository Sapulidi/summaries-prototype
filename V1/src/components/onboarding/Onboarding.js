import { Component } from 'preact';

import CheckboxButton from '../checkboxbutton/Checkboxbutton'
import style from './style';

export default class Onboarding extends Component {

    constructor(props) {
        super(props);

        this.state = {
            topic : 0,
            scrollPercentage: 0,
            topics: this.props.data.topics,
            topicClicks: 0
		}

        this.onPrevious = this.onPrevious.bind(this);
        this.onNext = this.onNext.bind(this);
        
    }

    onPrevious() {
        window.scrollTo(0, 0);
        this.setState({
            topic: this.determinePreviousTopic(),
            topicClicks: this.state.topicClicks -1
        })
    }

    onNext() {
        window.scrollTo(0, 0);
        this.setState({
            topic: this.determineNextTopic(),
            topicClicks: this.state.topicClicks +1
        })
    }

    determinePreviousTopic() {
        if(this.state.topic == 0) {
            return this.state.topics.length -1
        } else {
            return this.state.topic -1
        }
    }

    determineNextTopic() {
        if( this.state.topic + 1 >= this.state.topics.length ) {
            return 0
        } else {
            return this.state.topic + 1
        }
    }

	render() {
        const topics = this.state.topics;

		return (
            <div id="app" class={style.app}>
                <CheckboxButton text="button 1" />
            </div>
		);
	}
}
