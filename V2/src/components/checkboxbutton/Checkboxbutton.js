import { Component } from 'preact';
import style from './style';
export default class Checkboxbutton extends Component {

	constructor(props) {
		super(props);
		this.state = {
			checked: this.props.isChecked,
			disabled: this.props.disabled
		}
		this.checkButton = this.checkButton.bind(this);
	}

	checkButton() {
		this.props.setButtonClickedState();
		if(this.state.checked === "unchecked") {
			this.setState({
				checked: "checked"
			})
		} else if (this.state.checked === "checked") {
			this.setState({
				checked: "unchecked"
			})
		}
	}

	render() {
		return(
			<button onClick={this.checkButton} className={this.props.buttonClass ? this.props.buttonClass : ""} data-checked={this.state.checked} data-disabled={this.state.disabled}>{this.props.text}</button>
		)
	}
};