import { Component } from 'preact';
import Onboarding from './onboarding/Onboarding';

export default class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			optionSelected : false,
			data : null
		}
		this.getdata = this.getUserData.bind(this);
	}

	getUserData(e) {
		const data = require(`./data/${e.target.value}.json`);

		this.setState({
			data : data,
			optionSelected : true
		})
	}

	render() {
		const select = (
			<div>
				<h1>Selecteer gebruikersdata</h1>
				<p>Voeg de app toe aan beginscherm via chrome (mobile) en start de app vanuit je homescreen.</p>
				<select onChange={this.getdata} value="">
					<option value="user1">Any user</option>
				</select>
			</div>
			
		);
		return (
			!this.state.optionSelected ? <div>{select}</div> : <Onboarding data={this.state.data} />
		);
	}
}
