import { Component } from 'preact';

import CheckboxButton from '../checkboxbutton/Checkboxbutton'
import Home from '../home/Home'
import style from './style';

export default class Onboarding extends Component {

    constructor(props) {
        super(props);

        this.state = {
            topics: this.props.data.topics,
            data: this.props.data,
            buttonClicked: false
        }
        
    }

    setButtonClickedState(theButtonHasBeenClicked) {
        if(theButtonHasBeenClicked) {
            this.setState({
                buttonClicked : this.state.buttonClicked ? false : true
            })
        }
    }


	render() {
        const buttons = (
            <div class="onboarding">
                <h1>Hi!</h1>
                <p>Selecteer de onderwerpen die jou interesseren. We hebben er alvast een paar speciaal voor jou geselecteerd!</p>
                <ul>
                    <li>
                        <CheckboxButton 
                            setButtonClickedState={() => this.setButtonClickedState(false)}
                            text="Ondernemen" 
                            isChecked="checked"
                        />
                    </li>
                    <li>
                        <CheckboxButton 
                          text="Duurzaamheid" 
                          setButtonClickedState={() => this.setButtonClickedState(false)}
                          isChecked="checked"
                        />
                    </li>
                    <li>
                        <CheckboxButton 
                            text="Economie"
                            setButtonClickedState={() => this.setButtonClickedState(false)}
                            isChecked="unchecked"
                        />
                    </li> 
                    <li>
                        <CheckboxButton 
                            text="Zuid-Afrika"
                            setButtonClickedState={() => this.setButtonClickedState(false)}
                            isChecked="unchecked"
                        />
                    </li>
                    <li>
                        <CheckboxButton 
                             text="Recruitment"
                             setButtonClickedState={() => this.setButtonClickedState(false)}
                             isChecked="unchecked"
                        />
                    </li>
                    <li>
                        <CheckboxButton 
                             text="Technologie"
                             setButtonClickedState={() => this.setButtonClickedState(false)}
                             isChecked="unchecked"
                        />
                    </li>
                    <li>
                        <CheckboxButton 
                             text="Trump"
                             setButtonClickedState={() => this.setButtonClickedState(false)}
                             isChecked="unchecked"
                        />
                    </li>
                </ul>
                <hr />
                <CheckboxButton 
                    text="All set!"
                    setButtonClickedState={() => this.setButtonClickedState(true)}
                    buttonClass="confirm"
                    isChecked="unchecked"
                />
            </div>
            
        )

		return (
            <div id="app" class={style.app}>
                {!this.state.buttonClicked ? buttons : <Home data={this.props.data}/>}
            </div>
		);
	}
}
