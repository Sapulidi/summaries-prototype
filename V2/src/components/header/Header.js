import { Component } from 'preact';
import style from './style';

export default class Header extends Component {

	constructor(props) {
		super(props);
		this.state = {
			headerState : "closed"
		}
	}

	onHeaderClick() {
		if(this.state.headerState === "closed") {
			this.setState({
				headerState: "opened"
			})
		} else if (this.state.headerState === "opened") {
			this.setState({
				headerState: "closed"
			})
		}
	}
	
	render() {		
		return (
			<header data-headerState={this.state.headerState} class={style.header}>
				<h1 data-color={`color${this.props.headerColorClass}`}>{this.props.topic.title}</h1>
				<h2>{this.props.topic.readTime} minuten leestijd</h2>
				<div class="additional-content">
					<div class="content">
						<div class="bnr">
							<h1>{this.props.additionalInfo.bnr.title}</h1>
							<iframe class="no-reload" style="width:100%; min-height:145px; border:none;"src="https://widgets.bnr.nl/audio-widget/index.html?show=https://www.bnr.nl/podcast/json/10347347&theme=dark"></iframe>
						</div>
						<div class="wikipedia">
							<h1>{this.props.additionalInfo.WikiPedia.title}</h1>
							<p>“Duurzaam ondernemen is het leveren van concurrerend geprijsde goederen en diensten, die in de behoefte van de mens voorzien en die kwaliteit aan het leven geven, waarbij geleidelijk de milieubelasting en het grondstof- en energiegebruik door de levenscyclus en in de keten gereduceerd worden tot een niveau dat tenminste in balans is met de draagkracht van de aarde.”</p>
						</div>
						<div class="ftlexicon">
							<h1>{this.props.additionalInfo.FTLexicon.title}</h1>
							<p>Business sustainability represents resiliency over time – businesses that can survive shocks because they are intimately connected to healthy economic, social and environmental systems. 
								
								These businesses create economic value and contribute to healthy ecosystems and strong communities. There are a number of best practices that foster business sustainability, and help organisations move along the path from laggards to leaders. These practices include:</p>
							<ul>
								<li>Stakeholder engagement: Organisations can learn from customers, employees and their surrounding community. Engagement is not only about pushing out messages, but understanding opposition, finding common ground and involving stakeholders in joint decision-making;</li>
								<li>Environmental management systems: These systems provide the structures and processes that help embed environmental efficiency into a firm’s culture and mitigate risks. The most widely recognized standard worldwide is ISO 14001, but numerous other industry-specific and country-specific standards exist;</li>
								<li>Reporting and disclosure: Measurement and control are at the heart of instituting sustainable practices. Not only can organisations collect and collate the information, they can also be entirely transparent with outsiders. The Global Reporting Initiative is one of many examples of well-recognised reporting standards;</li>
								<li>Life cycle analysis: Those organisations wanting to take a large leap forward should systematically analyse the environmental and social impact of the products they use and produce through life cycle analysis, which measure more accurately impacts. Firms that are sustainable have been shown to attract and retain employees more easily and experience less financial and reputation risk. These firms are also more innovative and adaptive to their environments.</li>
							</ul>
						</div>
						<div class="energeia">
							<h1>{this.props.additionalInfo.Energeia.title}</h1> 
							<a href="https://energeia.nl/energeia-artikel/40071212/zomerserie-pioniers-een-duurzaam-huis-kan-echt-drie-tot-vier-keer-zo-goedkoop">ZOMERSERIE Pioniers - 'Een duurzaam huis kan écht drie tot vier keer zo goedkoop'</a>
							<a href="https://energeia.nl/energeia-artikel/40069016/waterstof-essentieel-voor-succes-van-wind-op-zee-en-energietransitie">Hans van der Lugt • Nieuws 'Waterstof essentieel voor succes van wind op zee en energietransitie'</a>
						</div>
						<div class="companyinfo">
							<h1>{this.props.additionalInfo.CompanyInfo.title}</h1>
							<div class="item">
								<h2>ExxonMobil Chemical Holland B.V.</h2>
								<h2>Omzet (2016): €1,8 Mld. | Winst (2016): € 71,4 Mln. | Werknemers (2016): 335</h2>
								<p>ExxonMobil Chemical Holland B.V. is opgericht in Rotterdam in 1979 en maakt onderdeel uit van de wereldwijde groep Exxon mobil Corporation, een Amerikaanse onderneming. Per 1 januari 2004 is de onderneming samengegaan met ExxonMobil Chemical Holland LLC en Mobil Chemical Amsterdam Inc. De onderneming wordt voortgezet onder de naam ExxonMobil Chemical Holland B.V. De kernactiviteit van de onderneming is de aankoop, productie, transport en verkoop van chemische producten.</p>
							</div>
							<div class="item">
								<h2>Koninklijke Luchtvaart Maatschappij N.V.</h2>
								<h2>Omzet (2017): € 10,3 Mld. | Winst (2017): € -704 Mln. | Werknemers (2017): 29,398</h2>
								<p>KLM together with its subsidiaries has as its principal business activities the air transport of passengers and cargo, aircraft maintenance and any other activity linked to air transport.</p>
							</div>
							<div class="item">
								<h2>Essent N.V.</h2>
								<h2>Omzet (2017): € 2,4 Mld. | Winst (2017): € 258,6 Mln. | Werknemers (2017): 2,468</h2>
								<p>Essent N.V. is een energiebedrijf met een groot pakket aan energie- en kabelproducten. Productie, handel, distributie en verkoop van energie zijn de kernactiviteiten. Daarnaast biedt men afvalmanagement- en internet-diensten aan. Essent is in 2009 overgenomen door het Duitse energiebedrijf RWE.</p>
							</div>
							<div class="item">
								<h2>BlackRock (Netherlands) B.V.</h2>
								<h2>Omzet (2017): € 1,1 Mln. | Winst (2017): € -184 K | Werknemers (2017): 50 t/m 99</h2>
								<p>BlackRock (Netherlands) B.V. heeft ten doel het verlenen van diensten met betrekking tot het vermogensbeheer en de administratieve uitvoering van pensioenregelingen van ondernemingen.</p>
							</div>
						</div>
					</div>
					<img onClick={() => this.onHeaderClick()} className="chevron" src="../../assets/img/chevron-right-solid.svg" />
				</div>
			</header>
		)
	}
};
