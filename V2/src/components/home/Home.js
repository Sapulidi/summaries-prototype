import { Component } from 'preact';


import Header from '../header/Header';
import SummaryList from '../summarylist/SummaryList';
import Footer from '../footer/Footer';
import style from './style';

export default class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            topic : 0,
            scrollPercentage: 0,
            topics: this.props.data.topics,
            topicClicks: 0
		}

        this.onPrevious = this.onPrevious.bind(this);
        this.onNext = this.onNext.bind(this);
        
    }

    onPrevious() {
        window.scrollTo(0, 0);
        this.setState({
            topic: this.determinePreviousTopic(),
            topicClicks: this.state.topicClicks -1
        })
    }

    onNext() {
        window.scrollTo(0, 0);
        this.setState({
            topic: this.determineNextTopic(),
            topicClicks: this.state.topicClicks +1
        })
    }

    determinePreviousTopic() {
        if(this.state.topic == 0) {
            return this.state.topics.length -1
        } else {
            return this.state.topic -1
        }
    }

    determineNextTopic() {
        if( this.state.topic + 1 >= this.state.topics.length ) {
            return 0
        } else {
            return this.state.topic + 1
        }
    }

	render() {
        const topics = this.state.topics;

		return (
            <div id="app" class={style.app}>
                <Header 
                    topic={topics[this.state.topic]}
                    headerColorClass={this.state.topic}
                    additionalInfo={this.props.data.additionalInfo}
                />

                <SummaryList topic={topics} topicIndex={this.state.topic} topicClicks={this.state.topicClicks}/>

                <Footer
                    onPrevious={this.onPrevious}
                    onNext={this.onNext}
                    previousTopicTitle={topics[this.determinePreviousTopic()].title}
                    nextTopicTitle={topics[this.determineNextTopic()].title}
                    scrollPercentage={this.state.scrollPercentage}
                    headerColorClass={this.state.topic}
                 />
            </div>
		);
	}
}
