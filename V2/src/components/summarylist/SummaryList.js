import { Component } from 'preact';
import ReactSwipe from 'react-swipe';
import style from './style';


export default class ArticleList extends Component {

	constructor(props) {
		super(props);
		this.props = props;

		this.swipeOptions = {
			continuous: false, 
			callback(index, elem) {
				this.callBackFunction(index, elem);
			}
		}
	}

	render() {
		const summaries = [];
		for (var i = 0; i < this.props.topic[this.props.topicIndex].summaries.length; i++) {
			summaries.push(
				{ "id" : "_" + Math.random().toString(36).substr(2, 9), "text" : this.props.topic[this.props.topicIndex].summaries[i], "url": this.props.topic[this.props.topicIndex].articleUrls[i] }
			);
		}

		// if( this.props.topicClicks > 4 ) {
		// 	summaries.push(
		// 		{ "id" : "_" + Math.random().toString(36).substr(2, 9), "text" : "Dit is een samenvatting die je nog niet had gelezen." },
		// 		{ "id" : "_" + Math.random().toString(36).substr(2, 9), "text" : "Dit is nog een samenvatting die je nog niet had gelezen." }
		// 	)
		// }

		const list = summaries.map(summary => (
			<div>
				{/* <ReactSwipe key={summary.id} swipeOptions={this.swipeOptions}> */}
				<a href={summary.url}><article>{summary.text}</article></a>	
				{/* </ReactSwipe> */}
				<hr />
			</div>
		));
		return(
			<section>{list}</section>
		)
	}
};


